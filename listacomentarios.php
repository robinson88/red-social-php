<!DOCTYPE html>
<?php
session_start();
require "conexion.php";
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/estilos.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Comentarios</title>
</head>
<body>
  <!-- aqui empieza modal comentario de publicaciones  -->
  <div class="modal" tabindex="-1" id="modalcomentarios">
                    <div class="modal-dialog">
                        <div class="modal-content">
                             <?php
                                     
                                    $consultaC="SELECT * FROM comentarios WHERE idpubli= '".$fila['id']."' ORDER BY id DESC";
                                    $resultado=mysqli_query($con,$consultaC);
                                    while ($rowC=$resultado->fetch_assoc()) {

                                        $queryC=mysqli_query($con, "SELECT * FROM registros WHERE id_usu = '".$rowC['idusuario']."'");
                                        $filaC=$queryC->fetch_array();
                                        $cont=mysqli_num_rows($resultado);
                                        
                                        $dato=printf($cont);
                                        
                                        ?> 
                                        <div class="publicaciones">
                                                    <div class="row">
                                                            <div class="col-auto foto">
                                                                <a href=""> <img class="imagenpublicacion" src="<?php echo ($filaC['avatar'])?>" alt=""> </a>
                                                            </div>
                                                            <div class="col post">
                                                                <a class="nombre"href=""><?php  echo $filaC['Nombre']?></a>
                                                                <p class="texto"><?php  echo $rowC['comentario']?></p>
                                                            </div>
                                                        </div>
                                                </div>
                                        <?php
                                        
                                    }
                                ?>
                        </div>
                    </div>
                </div>
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>